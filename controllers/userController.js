const User = require("../model/User");
const Order = require("../model/Order");
const bcrypt = require("bcrypt");
const auth =require("../auth");

module.exports.registerUser = (reqBody) =>{
    let newUser = new User({
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password,10)
    })
    return newUser.save().then((user,error) =>{
        if(error){
            return false
        } else {
            return true
        }
    })
}

module.exports.loginUser =(reqBody) =>{
    return User.findOne({email : reqBody.email}).then(result =>{
        if(result == null){
            return false;
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
            if(isPasswordCorrect){
                return {access : auth.createAccessToken(result)}
            }else{
                return false
            }
        }
    })
}
module.exports.getProfile = (data) => {
    console.log(data);
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	})
}
module.exports.makeAdmin =(reqParams,data) =>{
    let makingAdmin ={
        isAdmin : "true"
    }
    if(data.isAdmin){
    return User.findByIdAndUpdate(reqParams.userId,makingAdmin).then((
        user, error)=>{
            if(error){
                return false;
            }else{
                return true;
            }
        })
    }
    let message = Promise.resolve("User must be a admin for to access this!");
	return message.then((value) => {
		return value;
	});
    }


module.exports.getAllOrders =(data) =>{
    if(data.isAdmin){
    return Order.find({}).then(result =>{
        return result;
    })
    }
    let message = Promise.resolve("User must be a admin for to access this!");
	return message.then((value) => {
		return value;
	});
}
