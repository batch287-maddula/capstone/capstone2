const Order = require("../model/Order");
const Product = require("../model/Product")

module.exports.newOrder = async(data) => {
	let sum = 0;
    let i = 0;
	for(i; i< data.order.length; i++){
        let productprice = await Product.findById(data.order[i].productId).then(result =>{
        let price = result.price; 
        sum=sum+(data.order[i].quantity*price);
    	})}
	if(!(data.isAdmin)){
		let newOrder = new Order({
			userId: data.userId,
			products: data.order,
			totalAmount: sum

		});
		return newOrder.save().then((order,err) => {
			if(err){
				return "Error"
			} else{
				return "Order Successful!"
			};
		});
	};
	let message = Promise.resolve("User must be a non-admin for to access this!");
	return message.then((value) => {
		return value;
	});
};

