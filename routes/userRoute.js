const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth")

router.post("/register",(req,res) =>{
    userController.registerUser(req.body).then(resultFromController => res.send(
        resultFromController
    ));
});

module.exports = router;

router.post("/login",(req,res) =>{
    userController.loginUser(req.body).then(resultFromController => res.send(
        resultFromController
    ));
});
router.get("/:userId/userDetails",auth.verify,(req, res) => {
    const userData =auth.decode(req.headers.authorization);
    userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
})
router.put("/:userId/setAsAdmin",auth.verify,(req,res)=>{
    let data={
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    };
    userController.makeAdmin(req.params,data).then(resultFromController => res.send(
        resultFromController));
});

router.get("/orders",auth.verify,(req,res)=>{
    let data={
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    };
    userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});
module.exports = router;