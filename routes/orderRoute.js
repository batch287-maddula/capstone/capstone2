const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController")
const auth = require("../auth")


router.post("/newOrder", auth.verify, (req,res) => {

	let data = {
        userId : auth.decode(req.headers.authorization).id,
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	orderController.newOrder(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
