const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoute.js");
const productRoutes = require("./routes/productRoute.js");
const orderRoutes = require("./routes/orderRoute.js");
const app = express();
const cors = require("cors")
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.qatv1d1.mongodb.net/capstone-2",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open", () => console.log('Now connected in the cloud.'))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});

app.use("/users",userRoutes);
app.use("/products",productRoutes);
app.use("/orders",orderRoutes);


// admin eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0Yjc5ZDI0ZjdjZTZlYzdmMzhjM2RjMiIsImVtYWlsIjoiYWRtaW5AbWFpbC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2OTAzMDI0NDh9.HIjE-kfkYL6lLR0Wvl7SnnG7CGOApd8gEUIMuIiOmZ0
// nonadmin eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0Yjc5ZDU4ZjdjZTZlYzdmMzhjM2RjNCIsImVtYWlsIjoibm90YWRtaW5AbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjkwMzUyOTg5fQ.nmNMBNlF33KNtQ5FJylI3sSl_ixzMj9zPXTzIV62uMk